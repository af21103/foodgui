import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton Rice_ballButton;
    private JButton PizzaButton;
    private JButton udonButton;
    private JButton CremeCaramelButton;
    private JButton SaladButton;
    private JButton MonochromeCremeCaramelButton;
    private JLabel OrderLabel;
    private JButton checkout;
    private JLabel TotalLabel;
    private JTextArea OrderedItems;
    private JLabel TitleLabel;
    private JLabel Total;

    private int price=0;
    private int total_price=0;

    public FoodGUI() {
        Rice_ballButton.setIcon(new ImageIcon(
                this.getClass().getResource("onigiri.png")
        ));
        Rice_ballButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=120;
                order("Rice ball",price);
            }
        });
        PizzaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Pizza.png")
        ));
        PizzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=800;
                order("Pizza",price);
            }
        });
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon.png")
                ));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=550;
                order("Udon",price);
            }

        });
        SaladButton.setIcon(new ImageIcon(
                this.getClass().getResource("salad.png")
        ));
        SaladButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=380;
                order("Salad",price);
            }
        });
        CremeCaramelButton.setIcon(new ImageIcon(
                this.getClass().getResource("purin.png")
        ));
        CremeCaramelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=400;
                order("Creme caramel",price);
            }
        });
        MonochromeCremeCaramelButton.setIcon(new ImageIcon(
                this.getClass().getResource("purin_old.png")
        ));
        MonochromeCremeCaramelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                price=600;
                order("Monochrome creme caramel",price);
            }
        });
        checkout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CheckOut();
            }
        });
    }

    void order(String food, int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?\n" + "This is " + price + " yen.",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
            String currentText = OrderedItems.getText();
            OrderedItems.setText(currentText + food +"    "+price+" yen\n");
            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");
            total_price += price;
            TotalLabel.setText("Total   " + total_price + " yen");
        }
    }

    public void CheckOut(){

        if(total_price==0){
            JOptionPane.showMessageDialog(
                    null,
                    "Please select menu."
            );
            return;
        }
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to check out?\n",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
            JOptionPane.showMessageDialog(
                    null,
                    "Thank you!\n"+"The total price is "+total_price+" yen.\n"
            );
            OrderedItems.setText(null);
            total_price=0;
            TotalLabel.setText("Total " + total_price + " yen");
            Total.setText("Total       "+total_price+" yen");

        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
